import os
from flask import Flask, request, redirect, url_for
from pprint import pprint
from . import prefix

app = Flask(__name__)
app.wsgi_app = prefix.PrefixMiddleware(
        app.wsgi_app,
        prefix=os.environ.get("URL_PREFIX", '')
        )


@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        data = request.get_json()
        pprint(data)
        redirect(url_for('home'))
    return 'hello gluco world'


if __name__ == "__main__":
    app.run(port=os.environ.get("FLASK_RUN_PORT", ""), debug=True)
